*******************************
README
*******************************

Use and Implementation of the "ees_bootloader"

****************
General
****************
The ees_bootloader project is a standalone bootloader implemented for the ees circuit board.  Each project build is matched to a specific PCA so that the configuration bits, PPS configuration, port assignements, and clock schemes match the application to be loaded.

****************
Compiler Notes
****************
The base .hex file for the bootloader includes the btl_mz.ld linker script.  Any rebuild of the project should include the bootloader linker script to appropriately place the reset and interrupt vectors.  The project targets the PIC32MZ1024EFG100 component.

****************
Operation
****************
Once loaded to the PCA, the bootloader is fully functional on its own.

The USB port is live immediatelty.  A USB drive inserted before power-on OR during activity will both be recognized.  The bootloader uses auto-mount to read USB drives before or after the bootloader activates.  If the proper filename is available on the drive, the file is opened and the code uploaded to the program flash.  Once code is loaded, the application is launched.  If an application is already loaded to the part on device reset, the bootloader will first check for a flash file on the USB drive.  If there is a proper flash file present, the code will be updated.  If no file is present or if the USB drive is not available, after a short period of waiting for USB drive initialization, the timeout will expire and the bootloader will launch the existing application.  If no application is present and no USB drive is available or no flash file exists, the bootloader will continue to wait for a valid USB drive to be inserted with the proper flash file.

***************
USB MSD 
***************

USB drives should be formatted in the FAT32 format.  exFAT formatting may also be acceptable.  Currently all Lexar branded USB drives are recognized.  Additional drive compatibility is intermittent.  Drives 4GB and less are commonly accepted.  Over 4GB may require testing.

********************
External Indicators
********************
Normal bootloader indicators are two LEDS; a red and a green.

Typical operation for the bootloader will indicate error codes using the following LED pattern:

1 Red Flash = No Error
2 Red Flashes = No Code Present
3 Red Flashes = USB communication has failed
4 Red Flashes = Application has failed to launch

Blinking Green LED = FW being loaded to the program flash
Solid Green LED = FW has loaded successfully to the program flash

*********************************
Forcing Bootload from Application
*********************************
Using the SYS_RESET_SoftwareReset() command will cause the device to undergo a software reset. This will cause the bootloader to be entered.  If a USB drive with valid flash is present, the program flash will be updated.  If not, the current application will be loaded.

***********************
Building Applications
***********************
Applications that are intended to be loaded via the ees_bootloader must comply with a specific configuration, PPS arrangement, port configuration, clocking scheme, and must also be built using a linker script that correctly positions the application in program flash, the correct placement for the reset vector, and the correct placement for interrupt vectors.  Such a script is available and is referenced as app_mz.ld