#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-EESBootloaderBettaConfig.mk)" "nbproject/Makefile-local-EESBootloaderBettaConfig.mk"
include nbproject/Makefile-local-EESBootloaderBettaConfig.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=EESBootloaderBettaConfig
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/system_config/EESBootloaderBettaConfig/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon.c ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/EESBootloaderBettaConfig/framework/system/ports/src/sys_ports_static.c ../src/system_config/EESBootloaderBettaConfig/framework/system/reset/src/sys_reset.c ../src/system_config/EESBootloaderBettaConfig/system_init.c ../src/system_config/EESBootloaderBettaConfig/system_interrupt.c ../src/system_config/EESBootloaderBettaConfig/system_exceptions.c ../src/system_config/EESBootloaderBettaConfig/system_tasks.c ../src/main.c ../../../../framework/bootloader/src/datastream/datastream_usb_host.c ../../../../framework/bootloader/src/nvm.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_host.c ../../../../framework/system/fs/src/dynamic/sys_fs.c ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c ../../../../framework/system/fs/fat_fs/src/file_system/ff.c ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c ../../../../framework/system/int/src/sys_int_pic32.c ../../../../framework/system/tmr/src/sys_tmr.c ../../../../framework/usb/src/dynamic/usb_host.c ../../../../framework/usb/src/dynamic/usb_host_msd.c ../../../../framework/usb/src/dynamic/usb_host_scsi.c ../../../../framework/bootloader/src/ees_bootloader.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/1176562847/sys_devcon.o ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o ${OBJECTDIR}/_ext/650938363/sys_reset.o ${OBJECTDIR}/_ext/1100572353/system_init.o ${OBJECTDIR}/_ext/1100572353/system_interrupt.o ${OBJECTDIR}/_ext/1100572353/system_exceptions.o ${OBJECTDIR}/_ext/1100572353/system_tasks.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o ${OBJECTDIR}/_ext/1606335029/nvm.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ${OBJECTDIR}/_ext/66287330/ff.o ${OBJECTDIR}/_ext/2072869785/diskio.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ${OBJECTDIR}/_ext/610166344/usb_host.o ${OBJECTDIR}/_ext/610166344/usb_host_msd.o ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d ${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d ${OBJECTDIR}/_ext/650938363/sys_reset.o.d ${OBJECTDIR}/_ext/1100572353/system_init.o.d ${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d ${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d ${OBJECTDIR}/_ext/1100572353/system_tasks.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d ${OBJECTDIR}/_ext/1606335029/nvm.o.d ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d ${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d ${OBJECTDIR}/_ext/2104899551/sys_fs.o.d ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d ${OBJECTDIR}/_ext/66287330/ff.o.d ${OBJECTDIR}/_ext/2072869785/diskio.o.d ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d ${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d ${OBJECTDIR}/_ext/610166344/usb_host.o.d ${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/1176562847/sys_devcon.o ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o ${OBJECTDIR}/_ext/650938363/sys_reset.o ${OBJECTDIR}/_ext/1100572353/system_init.o ${OBJECTDIR}/_ext/1100572353/system_interrupt.o ${OBJECTDIR}/_ext/1100572353/system_exceptions.o ${OBJECTDIR}/_ext/1100572353/system_tasks.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o ${OBJECTDIR}/_ext/1606335029/nvm.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ${OBJECTDIR}/_ext/66287330/ff.o ${OBJECTDIR}/_ext/2072869785/diskio.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ${OBJECTDIR}/_ext/610166344/usb_host.o ${OBJECTDIR}/_ext/610166344/usb_host_msd.o ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o

# Source Files
SOURCEFILES=../src/system_config/EESBootloaderBettaConfig/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon.c ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/EESBootloaderBettaConfig/framework/system/ports/src/sys_ports_static.c ../src/system_config/EESBootloaderBettaConfig/framework/system/reset/src/sys_reset.c ../src/system_config/EESBootloaderBettaConfig/system_init.c ../src/system_config/EESBootloaderBettaConfig/system_interrupt.c ../src/system_config/EESBootloaderBettaConfig/system_exceptions.c ../src/system_config/EESBootloaderBettaConfig/system_tasks.c ../src/main.c ../../../../framework/bootloader/src/datastream/datastream_usb_host.c ../../../../framework/bootloader/src/nvm.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_host.c ../../../../framework/system/fs/src/dynamic/sys_fs.c ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c ../../../../framework/system/fs/fat_fs/src/file_system/ff.c ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c ../../../../framework/system/int/src/sys_int_pic32.c ../../../../framework/system/tmr/src/sys_tmr.c ../../../../framework/usb/src/dynamic/usb_host.c ../../../../framework/usb/src/dynamic/usb_host_msd.c ../../../../framework/usb/src/dynamic/usb_host_scsi.c ../../../../framework/bootloader/src/ees_bootloader.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-EESBootloaderBettaConfig.mk dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ1024EFG100
MP_LINKER_FILE_OPTION=,--script="..\src\system_config\EESBootloaderBettaConfig\btl_mz.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1176562847" 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1 -mdfp=${DFP_DIR}
	
else
${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1176562847" 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1176562847/sys_devcon_cache_pic32mz.o.asm.d",--gdwarf-2 -mdfp=${DFP_DIR}
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1456930456" 
	@${RM} ${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o ../src/system_config/EESBootloaderBettaConfig/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1176562847/sys_devcon.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1176562847" 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1176562847/sys_devcon.o ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1176562847" 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1541752504/sys_ports_static.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1541752504" 
	@${RM} ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o ../src/system_config/EESBootloaderBettaConfig/framework/system/ports/src/sys_ports_static.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/650938363/sys_reset.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/reset/src/sys_reset.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/650938363" 
	@${RM} ${OBJECTDIR}/_ext/650938363/sys_reset.o.d 
	@${RM} ${OBJECTDIR}/_ext/650938363/sys_reset.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/650938363/sys_reset.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/650938363/sys_reset.o.d" -o ${OBJECTDIR}/_ext/650938363/sys_reset.o ../src/system_config/EESBootloaderBettaConfig/framework/system/reset/src/sys_reset.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_init.o: ../src/system_config/EESBootloaderBettaConfig/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_init.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_init.o ../src/system_config/EESBootloaderBettaConfig/system_init.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_interrupt.o: ../src/system_config/EESBootloaderBettaConfig/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_interrupt.o ../src/system_config/EESBootloaderBettaConfig/system_interrupt.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_exceptions.o: ../src/system_config/EESBootloaderBettaConfig/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_exceptions.o ../src/system_config/EESBootloaderBettaConfig/system_exceptions.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_tasks.o: ../src/system_config/EESBootloaderBettaConfig/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_tasks.o ../src/system_config/EESBootloaderBettaConfig/system_tasks.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/826599022/datastream_usb_host.o: ../../../../framework/bootloader/src/datastream/datastream_usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/826599022" 
	@${RM} ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d" -o ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o ../../../../framework/bootloader/src/datastream/datastream_usb_host.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1606335029/nvm.o: ../../../../framework/bootloader/src/nvm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606335029" 
	@${RM} ${OBJECTDIR}/_ext/1606335029/nvm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606335029/nvm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606335029/nvm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1606335029/nvm.o.d" -o ${OBJECTDIR}/_ext/1606335029/nvm.o ../../../../framework/bootloader/src/nvm.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/246898221/drv_usbhs.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_host.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/2104899551/sys_fs.o: ../../../../framework/system/fs/src/dynamic/sys_fs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ../../../../framework/system/fs/src/dynamic/sys_fs.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o: ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/66287330/ff.o: ../../../../framework/system/fs/fat_fs/src/file_system/ff.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/66287330" 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o.d 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/66287330/ff.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/66287330/ff.o.d" -o ${OBJECTDIR}/_ext/66287330/ff.o ../../../../framework/system/fs/fat_fs/src/file_system/ff.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/2072869785/diskio.o: ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2072869785" 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o.d 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2072869785/diskio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/2072869785/diskio.o.d" -o ${OBJECTDIR}/_ext/2072869785/diskio.o ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1264926591/sys_tmr.o: ../../../../framework/system/tmr/src/sys_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1264926591" 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ../../../../framework/system/tmr/src/sys_tmr.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/610166344/usb_host.o: ../../../../framework/usb/src/dynamic/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_host.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_host.o ../../../../framework/usb/src/dynamic/usb_host.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/610166344/usb_host_msd.o: ../../../../framework/usb/src/dynamic/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_host_msd.o ../../../../framework/usb/src/dynamic/usb_host_msd.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/610166344/usb_host_scsi.o: ../../../../framework/usb/src/dynamic/usb_host_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o ../../../../framework/usb/src/dynamic/usb_host_scsi.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1606335029/ees_bootloader.o: ../../../../framework/bootloader/src/ees_bootloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606335029" 
	@${RM} ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d" -o ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o ../../../../framework/bootloader/src/ees_bootloader.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
else
${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1456930456" 
	@${RM} ${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1456930456/sys_clk_pic32mz.o ../src/system_config/EESBootloaderBettaConfig/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1176562847/sys_devcon.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1176562847" 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1176562847/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1176562847/sys_devcon.o ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1176562847" 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1176562847/sys_devcon_pic32mz.o ../src/system_config/EESBootloaderBettaConfig/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1541752504/sys_ports_static.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1541752504" 
	@${RM} ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1541752504/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/1541752504/sys_ports_static.o ../src/system_config/EESBootloaderBettaConfig/framework/system/ports/src/sys_ports_static.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/650938363/sys_reset.o: ../src/system_config/EESBootloaderBettaConfig/framework/system/reset/src/sys_reset.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/650938363" 
	@${RM} ${OBJECTDIR}/_ext/650938363/sys_reset.o.d 
	@${RM} ${OBJECTDIR}/_ext/650938363/sys_reset.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/650938363/sys_reset.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/650938363/sys_reset.o.d" -o ${OBJECTDIR}/_ext/650938363/sys_reset.o ../src/system_config/EESBootloaderBettaConfig/framework/system/reset/src/sys_reset.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_init.o: ../src/system_config/EESBootloaderBettaConfig/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_init.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_init.o ../src/system_config/EESBootloaderBettaConfig/system_init.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_interrupt.o: ../src/system_config/EESBootloaderBettaConfig/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_interrupt.o ../src/system_config/EESBootloaderBettaConfig/system_interrupt.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_exceptions.o: ../src/system_config/EESBootloaderBettaConfig/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_exceptions.o ../src/system_config/EESBootloaderBettaConfig/system_exceptions.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1100572353/system_tasks.o: ../src/system_config/EESBootloaderBettaConfig/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1100572353" 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1100572353/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1100572353/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1100572353/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1100572353/system_tasks.o ../src/system_config/EESBootloaderBettaConfig/system_tasks.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/826599022/datastream_usb_host.o: ../../../../framework/bootloader/src/datastream/datastream_usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/826599022" 
	@${RM} ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/826599022/datastream_usb_host.o.d" -o ${OBJECTDIR}/_ext/826599022/datastream_usb_host.o ../../../../framework/bootloader/src/datastream/datastream_usb_host.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1606335029/nvm.o: ../../../../framework/bootloader/src/nvm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606335029" 
	@${RM} ${OBJECTDIR}/_ext/1606335029/nvm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606335029/nvm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606335029/nvm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1606335029/nvm.o.d" -o ${OBJECTDIR}/_ext/1606335029/nvm.o ../../../../framework/bootloader/src/nvm.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/246898221/drv_usbhs.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o: ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/246898221" 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o.d" -o ${OBJECTDIR}/_ext/246898221/drv_usbhs_host.o ../../../../framework/driver/usb/usbhs/src/dynamic/drv_usbhs_host.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/2104899551/sys_fs.o: ../../../../framework/system/fs/src/dynamic/sys_fs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs.o ../../../../framework/system/fs/src/dynamic/sys_fs.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o: ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2104899551" 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d 
	@${RM} ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o.d" -o ${OBJECTDIR}/_ext/2104899551/sys_fs_media_manager.o ../../../../framework/system/fs/src/dynamic/sys_fs_media_manager.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/66287330/ff.o: ../../../../framework/system/fs/fat_fs/src/file_system/ff.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/66287330" 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o.d 
	@${RM} ${OBJECTDIR}/_ext/66287330/ff.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/66287330/ff.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/66287330/ff.o.d" -o ${OBJECTDIR}/_ext/66287330/ff.o ../../../../framework/system/fs/fat_fs/src/file_system/ff.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/2072869785/diskio.o: ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2072869785" 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o.d 
	@${RM} ${OBJECTDIR}/_ext/2072869785/diskio.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2072869785/diskio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/2072869785/diskio.o.d" -o ${OBJECTDIR}/_ext/2072869785/diskio.o ../../../../framework/system/fs/fat_fs/src/hardware_access/diskio.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1264926591/sys_tmr.o: ../../../../framework/system/tmr/src/sys_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1264926591" 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1264926591/sys_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1264926591/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/1264926591/sys_tmr.o ../../../../framework/system/tmr/src/sys_tmr.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/610166344/usb_host.o: ../../../../framework/usb/src/dynamic/usb_host.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_host.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_host.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_host.o ../../../../framework/usb/src/dynamic/usb_host.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/610166344/usb_host_msd.o: ../../../../framework/usb/src/dynamic/usb_host_msd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_msd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_host_msd.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_host_msd.o ../../../../framework/usb/src/dynamic/usb_host_msd.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/610166344/usb_host_scsi.o: ../../../../framework/usb/src/dynamic/usb_host_scsi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/610166344" 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d 
	@${RM} ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/610166344/usb_host_scsi.o.d" -o ${OBJECTDIR}/_ext/610166344/usb_host_scsi.o ../../../../framework/usb/src/dynamic/usb_host_scsi.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/1606335029/ees_bootloader.o: ../../../../framework/bootloader/src/ees_bootloader.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606335029" 
	@${RM} ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src/system_config/EESBootloaderConfig" -I"../src/EESBootloaderConfig" -I"../src/system_config/EESBootloaderConfig/framework" -I"../src" -I"../src/system_config/EESBootloaderBettaConfig" -I"../src/EESBootloaderBettaConfig" -I"../../../../framework" -I"../src/system_config/EESBootloaderBettaConfig/framework" -MMD -MF "${OBJECTDIR}/_ext/1606335029/ees_bootloader.o.d" -o ${OBJECTDIR}/_ext/1606335029/ees_bootloader.o ../../../../framework/bootloader/src/ees_bootloader.c    -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MZ1024EFG100_peripherals.a  ../src/system_config/EESBootloaderBettaConfig/btl_mz.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\..\..\bin\framework\peripheral\PIC32MZ1024EFG100_peripherals.a      -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x37F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=1024,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp=${DFP_DIR}
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MZ1024EFG100_peripherals.a ../src/system_config/EESBootloaderBettaConfig/btl_mz.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\..\..\bin\framework\peripheral\PIC32MZ1024EFG100_peripherals.a      -DXPRJ_EESBootloaderBettaConfig=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=1024,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp=${DFP_DIR}
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/EES_Bootloader.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/EESBootloaderBettaConfig
	${RM} -r dist/EESBootloaderBettaConfig

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
